import {useCallback, useEffect, useMemo, useState} from 'react';
import {useLocation} from "react-router-dom";
import {getParams} from "./utils/helpers";
import firebase from 'firebase/app'
import 'firebase/auth'
import {
  RESET_PASSWORD_FAILED, RESET_PASSWORD_FORM,
  RESET_PASSWORD_SUCCESS,
  VERIFY_EMAIL_FAILED,
  VERIFY_EMAIL_SUCCESS
} from "./utils/constants";
import {ResetPassword} from "./components/Reset-password";
import {ResetPasswordFail} from "./components/Reset-password-fail";
import {VerifyEmailFail} from "./components/Verify-email-fail";
import {VerifyEmailSuccess} from "./components/Verify-email-success";
import {ResetPasswordSuccess} from "./components/Reset-password-success";
import {Spinner} from "./components/Spinner";

const firebaseConfig = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_APP_ID,
  measurementId: process.env.REACT_APP_MEASUREMENT_ID
};

const app = firebase.initializeApp(firebaseConfig)
const auth = app.auth()

const App = () => {
  const location = useLocation()
  const [node, setNode] = useState("")

  const params = useMemo(() => {
    return getParams(location.search)
  }, [location.search])


  const onPasswordConfirmHandler = (password: string) => {
    if (password) {
      auth.confirmPasswordReset(params.actionCode, password)
          .then(() => {
            setNode(RESET_PASSWORD_SUCCESS)
          })
          .catch(() => {
            setNode(RESET_PASSWORD_FAILED)
          })
    }
  }

  const handleResetPassword = useCallback((auth: any, actionCode: string) => {
    auth.verifyPasswordResetCode(actionCode)
        .then(() => {
          setNode(RESET_PASSWORD_FORM)
        })
        .catch(() => {
          setNode(RESET_PASSWORD_FAILED)
        })
  }, [])

  const handleVerifyEmail = useCallback((auth: any, actionCode: string) => {
    auth.applyActionCode(actionCode)
        .then(() => {
          setNode(VERIFY_EMAIL_SUCCESS)
        })
        .catch(() => {
          setNode(VERIFY_EMAIL_FAILED)
        })
  }, [])

  useEffect(() => {
    if (params.mode === 'verifyEmail') {
      handleVerifyEmail(auth, params.actionCode)
    } else if(params.mode === 'resetPassword') {
      handleResetPassword(auth, params.actionCode)
    }
  }, [params, handleResetPassword, handleVerifyEmail])

  switch (node) {
    case RESET_PASSWORD_FAILED:
      return <ResetPasswordFail/>
    case VERIFY_EMAIL_FAILED:
      return <VerifyEmailFail/>
    case VERIFY_EMAIL_SUCCESS:
      return <VerifyEmailSuccess/>
    case RESET_PASSWORD_SUCCESS:
      return <ResetPasswordSuccess/>
    case RESET_PASSWORD_FORM:
      return <ResetPassword onChange={onPasswordConfirmHandler}/>
    default:
      return <Spinner/>
  }
}

export default App;
