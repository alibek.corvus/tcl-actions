const newPasswordResources = {
    tagline: "CREATE NEW PASSWORD",

    newPasswordLabel: "New Password",
    passwordHelperText:
        "Must be at least 8 characters and contain one uppercase (A-Z) and one number (0-9)",

    confirmNewPassword: "Confirm New Password",
    repeatPasswordHelperText: "Passwords do not match",

    savePassword: "SAVE NEW PASSWORD",
};

export default newPasswordResources;
