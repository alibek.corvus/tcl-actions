import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import './NewPassword.css';
import CustomInput from "../common/Input/CustomInput";
import newPasswordResources from "./newPasswordResources";
import Logo from "../common/Logo/Logo";
import {AnimatePresence, motion} from "framer-motion";
import signUp from '../assets/signup.png'

type TProps = {
    onChange: (val: string) => void
}

const NewPassword: React.FC<TProps> = React.memo(({onChange}) => {
    const [disabled, setDisabled] = useState<boolean>(true);
    const [password, setPassword] = useState<string>('');
    const [passwordInvalidity, setPasswordInvalidity] = useState<boolean | null>(null);
    const [repeatPassword, setRepeatPassword] = useState<string>('');
    const [repeatPasswordInvalidity, setRepeatPasswordInvalidity] = useState<boolean | null>(null);
    const [passwordsDoNotMatch, setPasswordsDoNotMatch] = useState<boolean | null>(null);

    const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        if (
            event.target.value.search(/[0-9]/) < 0 ||
            event.target.value.search(/[A-Z]/) < 0 ||
            event.target.value.length < 8
        ) {
            setPasswordInvalidity(true);
        } else setPasswordInvalidity(false);
        setPassword(event.target.value);
    };

    const onClickHandler = () => {
        if (password === repeatPassword) {
            onChange(password)
        }
    }

    const handleRepeatPasswordChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        if (
            event.target.value.search(/[0-9]/) < 0 ||
            event.target.value.search(/[A-Z]/) < 0 ||
            event.target.value.length < 8
        ) {
            setRepeatPasswordInvalidity(true);
        } else setRepeatPasswordInvalidity(false);
        setRepeatPassword(event.target.value);
    };

    useEffect(() => {
        if (passwordInvalidity === false && repeatPasswordInvalidity === false) {
            if (password === repeatPassword) {
                setPasswordsDoNotMatch(null);
                setDisabled(false);
            } else {
                setPasswordsDoNotMatch(true);
                setDisabled(true);
            }

            if (password.length !== repeatPassword.length) {
                setPasswordsDoNotMatch(true);
            }
        }

        if (repeatPassword.length === 0) {
            setPasswordsDoNotMatch(null);
        }
    }, [password, repeatPassword, passwordsDoNotMatch, passwordInvalidity, repeatPasswordInvalidity]);

    return (
        <main className='logging-page'>
            <AnimatePresence>
                <motion.img
                    initial={{ opacity: 0, position: 'absolute' }}
                    animate={{ opacity: 1, position: 'initial' }}
                    exit={{ opacity: 0, position: 'absolute' }}
                    transition={{ duration: 2 }}
                    className="professional-image"
                    src={signUp}
                    alt="Sign In background picture"
                />
            </AnimatePresence>
        <div className="new-password-page">
            <section className="new-password">
                <Logo/>
                <form className="new-password-form">
                    <h2 className="new-password-form-tagline">{newPasswordResources.tagline}</h2>
                    <div className="new-password-from-inputs">
                        <CustomInput
                            className="new-password-input-upper"
                            label={newPasswordResources.newPasswordLabel}
                            type="password"
                            handleChange={handlePasswordChange}
                            value={password}
                            helperText={newPasswordResources.passwordHelperText}
                            helperTextFlag={passwordInvalidity}
                        />
                        <CustomInput
                            className="new-password-input"
                            label={newPasswordResources.confirmNewPassword}
                            type="password"
                            handleChange={handleRepeatPasswordChange}
                            value={repeatPassword}
                            helperText={newPasswordResources.repeatPasswordHelperText}
                            helperTextFlag={passwordsDoNotMatch}
                        />
                    </div>
                    <Link className="new-password-form-button-link" to={'#'}>
                        <button className="new-password-form-button" onClick={onClickHandler} disabled={disabled}>
                            {newPasswordResources.savePassword}
                        </button>
                    </Link>
                </form>
                <p className="right-claim">© 2021 The Village Ledger. ALL RIGHTS RESERVED.</p>
            </section>
        </div>
        </main>
    );
});

export default NewPassword;
