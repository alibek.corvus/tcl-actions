import Wrapper from "./Wrapper";
import {stylesWrapper} from "./Reset-password-fail";
import {Button} from "react-bootstrap";

export const ResetPasswordSuccess = () => {
    const onReturnHandler = () => {
        window.open(process.env.REACT_APP_SIGN_IN_URL)
    }

    return (
        <Wrapper styles={stylesWrapper}>
            <h3>You have changed your password successfully</h3>
            <p>You can now sign in with your new password</p>
            <Button
                onClick={onReturnHandler}
                className="w-40 mt-2"
            >
                Go to TVL Pro
            </Button>
        </Wrapper>
    );
};
