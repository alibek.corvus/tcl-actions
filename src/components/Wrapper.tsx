import {FC, memo} from 'react';
import {Container} from "react-bootstrap";

type TProps = {
    styles?: {[keys: string]: string}
}

const Wrapper: FC<TProps> = memo(({children, styles}) => {
    return (
        <Container
            className="d-flex align-items-center justify-content-center"
            style={{
                minHeight: "100vh"
            }}
        >
            <div
                className="w-100"
                style={{
                    maxWidth: 400,
                    ...styles
                }}
            >
                {children}
            </div>
        </Container>
    );
});

export default Wrapper;