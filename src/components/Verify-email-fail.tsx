import Wrapper from "./Wrapper";
import {stylesWrapper} from "./Reset-password-fail";

export const VerifyEmailFail = () => {
    return (
        <Wrapper styles={stylesWrapper}>
            <h3>Please try your request again</h3>
            <p>Your email verification link has expired or has already been used. You can request a new verification email by going through Sign Up process one more time</p>
        </Wrapper>
    );
};