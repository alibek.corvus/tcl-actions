import Wrapper from "./Wrapper";
import {stylesWrapper} from "./Reset-password-fail";

export const VerifyEmailSuccess = () => {
    return (
        <Wrapper styles={stylesWrapper}>
            <h3>You have confirmed your password successfully</h3>
        </Wrapper>
    );
};


