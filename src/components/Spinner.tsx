import Wrapper from "./Wrapper";
import {Spinner as MySpinner} from "react-bootstrap";

export const Spinner = () => {
    return (
        <Wrapper styles={{marginLeft: '250px'}}>
            <MySpinner animation="grow"/>
        </Wrapper>
    );
}