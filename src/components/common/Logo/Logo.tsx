import React  from 'react';
import logoPro from '../../assets/logopro.jpg'
import logoName from '../../assets/logoname.svg'
import './Logo.css';

const Logo: React.FC = () => {
    return (
        <div className="sign-up-logo-tagline">
            <img
                className="logo"
                src={logoPro}
                alt="The Village Ledger logo"
            />
            <img
                className="tagline"
                src={logoName}
                alt="The Village Ledger tagline"
            />
        </div>
    );
}

export default Logo;
