import Wrapper from "./Wrapper";

export const stylesWrapper = {
    maxWidth: "1000",
    textAlign: 'center'
}

export const ResetPasswordFail = () => {
    return (
        <Wrapper styles={stylesWrapper}>
            <h3>Please try your request again</h3>
            <p>Your password reset link has expired or has already been used. You can request a new reset password link
                by going to the Sign in page and tapping the forgot password link</p>
        </Wrapper>
    );
};