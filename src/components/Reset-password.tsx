import {FC, memo, useState} from 'react';
import {Button, Card, Form} from "react-bootstrap";
import Wrapper from "./Wrapper";

type TProps = {
    onChange: (val: string) => void
}

export const ResetPassword: FC<TProps> = memo(({onChange}) => {
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')

    const onClickHandler = () => {
        if (password === confirmPassword) {
            onChange(password)
        }
    }
    return (
        <Wrapper>
            <Card>
                <Card.Body>
                    <h2 className="text-center mb-4">Reset Password</h2>
                    <Form>
                        <Form.Group id='password'>
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                value={password}
                                onChange={e => setPassword(e.target.value)}
                                type="password"
                                required
                            />
                        </Form.Group>
                        <Form.Group id='confirm-password'>
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                value={confirmPassword}
                                onChange={e => setConfirmPassword(e.target.value)}
                                type="password"
                                required
                            />
                        </Form.Group>
                        <Button
                            onClick={onClickHandler}
                            className="w-100 mt-2"
                        >
                            Reset
                        </Button>
                    </Form>
                </Card.Body>
            </Card>
        </Wrapper>
    );
});
