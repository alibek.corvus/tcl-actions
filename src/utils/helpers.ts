export const getParams = (search: string) => {
    const params = {} as {mode : string, actionCode: string}

    if (search) {
        const target = search.split('?')[1]
        target.split("&").forEach(it => {
            if (it.includes('mode')) {
                params.mode = it.split("=")[1]
            }
            if(it.includes("oobCode")) {
                params.actionCode = it.split("=")[1]
            }
        })
    }

    return params
}